﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
{
  class Program
  {
    static void Main(string[] args)
    {
      //declaring some variables upfront, so I can keep using them after my
      //db context has been disposed
      var customerAccountNumbers = new List<string>();
      var customerDetails = new List<Customer>();
      var customerChapmanDetails = new List<Customer>();
      var customerSalesAmountDetails = new List<Customer>();
      var salesOrderHeaderMaxFreight = new SalesOrderHeader();
      var customerMaxFreight = new Customer();

      //inside this using statement, db is your database connection; the source of all of your queries
      //outside of the using statement, the database connection no longer exists and is cleaned up
      //by the garbage collector

      using (var db = new AdventureWorks2014Entities())
      {
        //Query 1
        customerAccountNumbers = db.Customers.Select(x => x.AccountNumber).OrderBy(x => x).ToList();

        //Query 2, lambda version
        customerDetails = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ToList();

        #region Query 2, LINQ version
        ////Query 2, linq version
        //customerDetails = (from customer in db.Customers
        //                    join person in db.People on customer.PersonID equals person.BusinessEntityID into pj
        //                    from person in pj.DefaultIfEmpty()
        //                    join store in db.Stores on customer.StoreID equals store.BusinessEntityID into sj
        //                    from store in sj.DefaultIfEmpty()
        //                    join salesOrderHeaders in db.SalesOrderHeaders on customer.CustomerID equals salesOrderHeaders.CustomerID into soj
        //                    from salesOrderHeaders in soj.DefaultIfEmpty()
        //                    orderby person.LastName, person.FirstName
        //                    select customer)
        //  .Include(x => x.Person)
        //  .Include(x => x.Store)
        //  .Include(x => x.SalesOrderHeaders)
        //  .ToList();
        #endregion

        //Query 3, filtering data by person's last name
        customerChapmanDetails = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .Where(x => x.Person.LastName.Equals("Chapman"))
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ToList();

        //Query 4, filtering data by sales amount
        customerSalesAmountDetails = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .Where(x => x.SalesOrderHeaders.Sum(y => y.TotalDue) > (decimal)300000)
          .OrderBy(x => x.Person.LastName)
          .ThenBy(x => x.Person.FirstName)
          .ToList();

        //Query 5 (see freight % output in Queries 1-4)

        //Query 6, finding the customer that paid the most for freight

        //step 1, find the CustomerID of the customer that paid the most freight
        //by looking at just the SalesOrderHeaders
        salesOrderHeaderMaxFreight = db.SalesOrderHeaders.OrderByDescending(x => x.Freight / x.TotalDue).First();

        //step 2, use the above SalesOrderHeader to get data for one customer
        customerMaxFreight = db.Customers
          .Include(x => x.Person)
          .Include(x => x.Store)
          .Include(x => x.SalesOrderHeaders)
          .Where(x => x.CustomerID == salesOrderHeaderMaxFreight.CustomerID)
          .Single();
      }

      //Query 1 output
      var query1Path = @"../../Query1.txt";

      using (var query1Writer = new StreamWriter(query1Path))
      {
        FileWriteLine(query1Writer, "**** Customer Account Numbers ****");

        foreach (var acctNum in customerAccountNumbers)
        {
          FileWriteLine(query1Writer, acctNum);
        }

        FileWriteLine(query1Writer, "\r\n**** Number of Accounts ****");

        FileWriteLine(query1Writer, customerAccountNumbers.Count);
      }

      //Query 2 output
      var query2Path = @"../../Query2.txt";

      using (var query2Writer = new StreamWriter(query2Path))
      {
        FileWriteCustomers(query2Writer, customerDetails);

        FileWriteLine(query2Writer, "\r\n**** Number of Customers ****");

        FileWriteLine(query2Writer, customerDetails.Count);
      }
      
      //Query 3 output
      var query3Path = @"../../Query3.txt";

      using (var query3Writer = new StreamWriter(query3Path))
      {
        FileWriteCustomers(query3Writer, customerChapmanDetails);

        FileWriteLine(query3Writer, "\r\n**** Number of Customers ****");

        FileWriteLine(query3Writer, customerChapmanDetails.Count);
      }

      //Query 4 output
      var query4Path = @"../../Query4.txt";

      using (var query4Writer = new StreamWriter(query4Path))
      {
        FileWriteCustomers(query4Writer, customerSalesAmountDetails);

        FileWriteLine(query4Writer, "\r\n**** Number of Customers ****");

        FileWriteLine(query4Writer, customerSalesAmountDetails.Count);
      }

      //Query 6 output
      var query6Path = @"../../Query6.txt";

      using (var query6Writer = new StreamWriter(query6Path))
      {
        FileWriteCustomers(query6Writer, new List<Customer> { customerMaxFreight }, salesOrderHeaderMaxFreight);

        FileWriteLine(query6Writer, "\r\n**** Number of Customers ****");

        FileWriteLine(query6Writer, 1);
      }
    }

    private static void FileWriteCustomers(StreamWriter streamWriter, List<Customer> customers, SalesOrderHeader maxFreightSale = null)
    {
      foreach (var customer in customers)
      {
        var customerName = (customer.Person != null) ? string.Format("{0}, {1}", customer.Person.LastName, customer.Person.FirstName) : "NULL";
        var storeName = (customer.Store != null) ? customer.Store.Name : "NULL";

        //customer id
        FileWriteLine(streamWriter, string.Format("**** CustomerID {0} ****", customer.CustomerID));

        //customer name, store name
        FileWriteLine(streamWriter, string.Format("\tName = {0}; Store = {1}", customerName, storeName));

        if (customer.SalesOrderHeaders.Count > 0)
        {
          //sales data
          FileWriteLine(streamWriter, "\t**** Sales ****");

          foreach (var salesOrderHeader in customer.SalesOrderHeaders)
          {
            FileWriteSalesRecord(streamWriter, salesOrderHeader);
          }

          //sales total
          FileWriteLine(streamWriter, string.Format("\t\tSales Total = {0:C}", customer.SalesOrderHeaders.Sum(x => x.TotalDue)));

          //max freight sale
          if (maxFreightSale != null)
          {
            FileWriteLine(streamWriter, "\t\t**** Max Freight Found ****");
            FileWriteSalesRecord(streamWriter, maxFreightSale);
          }
        }
        else
        {
          FileWriteLine(streamWriter, "\t**** No Sales For This Customer ****");
        }
      }
    }

    private static void FileWriteSalesRecord(StreamWriter writer, SalesOrderHeader salesOrderHeader)
    {
      FileWriteLine(writer, string.Format("\t\tSubTotal = {0:C}; Tax = {1:C}; Freight = {2:C}; Total = {3:C}; Freight Percentage = {4:P}", salesOrderHeader.SubTotal, salesOrderHeader.TaxAmt, salesOrderHeader.Freight, salesOrderHeader.TotalDue, salesOrderHeader.Freight / salesOrderHeader.TotalDue));
    }

    private static void FileWriteLine(StreamWriter writer, object data)
    {
      writer.WriteLine(data);
      Console.WriteLine(data);
    }
  }
}
